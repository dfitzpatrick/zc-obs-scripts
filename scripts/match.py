import obspython as obs
import urllib.request
import urllib.error
import json
hotkey_id = obs.OBS_INVALID_HOTKEY_ID

def is_mapped_field(name):
    try:
        tokens = name.split('_')
        return tokens[-1] == 'match'
    except IndexError:
        return False

def is_team_player(name):
    try:
        tokens = name.split('_')
        return tokens[0][:-1] == 'team'
    except IndexError:
        return False

def mapped_field_to_key(name):
    # Assumes that _match is appended at the end. Remove it.
    try:
        tokens = name.split('_')
        return '_'.join(tokens[0:-1])
    except IndexError:
        return name

def truncate_name(name):
    # Overlay can support about 11 characters with current design on team names
    if len(name) > 11:
        name = ''.join(c for c in name[0:10]) + '..'
    return name

def match_hotkey_callback(pressed):
    update_sources()

def update_sources():
    resp = None
    url = "http://www.zc4proleague.com/api/currentmatchsummary"
    valid_text_fields = ['text_gdiplus', 'text_ft2_source']

    sources = obs.obs_enum_sources()
    text_fields = [obs.obs_source_get_name(tf) for tf in sources
                   if obs.obs_source_get_id(tf) in valid_text_fields]
    obs.source_list_release(sources)

    try:
        with urllib.request.urlopen(url) as response:
            data = response.read()
            context = json.loads(data)
    except urllib.error.URLError as err:
        obs.script_log(obs.LOG_WARNING, "Error opening URL '" + url + "': " + err.reason)
        obs.remove_current_callback()
        return


    for tf in text_fields:
        if not is_mapped_field(tf):
            continue
        tf_source = obs.obs_get_source_by_name(tf)
        ctx_value = context.get(mapped_field_to_key(tf))
    
        settings = obs.obs_data_create()
        obs.obs_data_set_string(settings, "text", "") # clear all

        if ctx_value is not None:
            obs.obs_data_set_string(settings, "text", str(ctx_value))
        obs.obs_source_update(tf_source, settings)
        obs.obs_data_release(settings)
        obs.obs_source_release(tf_source)


def script_load(settings):
    global hotkey_id
    hotkey_id = obs.obs_hotkey_register_frontend("match_teams_update.trigger", "Match Teams Update", match_hotkey_callback)
    hotkey_save_array = obs.obs_data_get_array(settings, "match_teams_update.trigger")
    obs.obs_hotkey_load(hotkey_id, hotkey_save_array)
    obs.obs_data_array_release(hotkey_save_array)

def script_save(settings):
    global hotkey_id
    hotkey_save_array = obs.obs_hotkey_save(hotkey_id)
    obs.obs_data_set_array(settings, "instant_replay.trigger", hotkey_save_array)
    obs.obs_data_array_release(hotkey_save_array)

def script_description():
    return "Grabs Match data from ZC League API"

def update_pressed(props, prop):
    return update_sources()

def script_properties():
    props = obs.obs_properties_create()
    obs.obs_properties_add_button(props, "button", "Update", update_pressed)
    return props
