import json
import os
from collections import Sequence
from pathlib import Path
from subprocess import Popen


def pause() -> str:
    return input('Press any key to close... ')

def build_path(path: str) -> str:
    base = os.path.dirname(os.path.realpath(__file__))
    return os.path.normpath(f"{base}/{path}")

def update_repository() -> None:
    try:
        git = Popen("git pull origin master").wait()
        print("Repository has been updated")
    except Exception as e:
        print(f"Error: {e}")
        pause()


def update_settings(d: dict) -> None:
    keys = ['file', 'local_file', 'path']
    for k,v in d.items():
        if k in keys:
            path = Path(v)
            if k == 'path':
                local_path = build_path(f"scripts/{path.name}")
                print(f"Changing {v} to {local_path}")
            else:
                local_path = build_path(f"assets/{path.name}")
                print(f"Changing {v} to {local_path}")
            d[k] = local_path
        elif isinstance(d[k], dict):
            update_settings(d[k])
        elif isinstance(d[k], Sequence):
            for i in d[k]:
                if isinstance(i, dict):
                    update_settings(i)



if __name__ == '__main__':
    JSON_FILENAME = 'Zone_Control_4.json'
    try:
        update_repository()
        with open(build_path(JSON_FILENAME), 'r') as f:
            settings = json.load(f)
        if settings:
            update_settings(settings)
            with open(build_path(JSON_FILENAME), "w") as f:
                json.dump(settings, f)
            print("Updated Settings Succesfully.")
        else:
            print("Error Loading Settings Module.")
    except Exception as e:
        print(f"Error: {e}")
    pause()
